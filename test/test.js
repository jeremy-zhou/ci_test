var should = require('chai').should(),
    expect = require('chai').expect,
    supertest = require('supertest'),
    api = supertest('http://localhost:8080');

describe('User', function () {

    var location1;
    var location2;
    var location3;
    var locations = [location1, location2, location3];

    before(function (done) {

        api.post('/api/task')
            .set('Accept', 'application/x-www-form-urlencoded')
            .send({
                title: "111 Main St",
                description: "Portland"
            })
            .expect('Content-Type', /json/)
            .expect(200)
            .end(function (err, res) {
                location1 = res.body;
            });


        api.post('/api/task')
            .set('Accept', 'application/x-www-form-urlencoded')
            .send({
                title: "222 Main St",
                description: "Portland 2"
            })
            .expect('Content-Type', /json/)
            .expect(200)
            .end(function (err, res) {
                location2 = res.body;
            });

        api.post('/api/task')
            .set('Accept', 'application/x-www-form-urlencoded')
            .send({
                title: "333 Main St",
                description: "Portland 3"
            })
            .expect('Content-Type', /json/)
            .expect(200)
            .end(function (err, res) {
                location3 = res.body;
                done();
            });
    });

    it('should return a 200 response', function (done) {
        api.get('/api/task/1')
            .set('Accept', 'application/json')
            .expect(200, done);
    });

    it('should be an object with keys and values', function (done) {
        api.get('/api/task/1')
            .set('Accept', 'application/json')
            .expect(200)
            .end(function (err, res) {
                expect(res.body).to.have.property("title");
                expect(res.body.title).to.not.equal(null);
                expect(res.body).to.have.property("description");
                expect(res.body.description).to.not.equal(null);
                done();
            });
    });

    it('should have a 10 digit phone number', function (done) {
        api.get('/api/task/1')
            .set('Accept', 'application/json')
            .expect(200)
            .end(function (err, res) {
                expect(res.body.description.length).to.equal(8);
                done();
            });
    });

    it('should have the role of admin', function (done) {
        api.get('/api/task/1')
            .set('Accept', 'application/json')
            .expect(200)
            .end(function (err, res) {
                expect(res.body.description).to.equal("Portland");
                done();
            });
    });

    it('should be updated with a new name', function (done) {
        api.put('/api/task/1')
            .set('Accept', 'application/x-www-form-urlencoded')
            .send({
                title: "Kevin",
                description: "kevin@example.com"
            })
            .expect(200)
            .end(function (err, res) {
                expect(res.body.title).to.equal("Kevin");
                expect(res.body.description).to.equal("kevin@example.com");
                done();
            });
    });
    //
    // it('should access their own locations', function (done) {
    //     api.get('/users/1/location')
    //         .set('Accept', 'application/x-www-form-urlencoded')
    //         .send({
    //             userId: 1
    //         })
    //         .expect(200)
    //         .end(function (err, res) {
    //             expect(res.body.userId).to.equal(1);
    //             expect(res.body.addressCity).to.equal("Portland");
    //             done();
    //         });
    // });
    //
    //
    // it('should not be able to access other users locations', function (done) {
    //     api.get('/users/2/location')
    //         .set('Accept', 'application/x-www-form-urlencoded')
    //         .send({
    //             userId: 1
    //         })
    //         .expect(401)
    //         .end(function (err, res) {
    //             if (err) return done(err);
    //             expect(res.error.text).to.equal("Unauthorized");
    //             done();
    //         });
    // });

});