var packageJSON = require('./package.json');
var path = require('path');
var webpack = require('webpack');

const PATHS = {
    build: path.join(__dirname, 'src', 'main', 'resources', 'static', 'js')
};

module.exports = {
    entry: {
        app: './src/main/web/index.js',
        vendor: ['angular']
    },

    output: {
        path: PATHS.build,
        filename: 'app.js'
    },

    plugins: [
        new webpack.optimize.CommonsChunkPlugin(/* chunkName= */"vendor", /* filename= */"vendor.bundle.js")
    ]
};